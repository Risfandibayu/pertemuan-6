package bayu.risfandi.pertemuan6

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() ,View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        textView_mendaftar.setOnClickListener(this)
        button_signin.setOnClickListener(this)
        supportActionBar!!.title = "Login"
    }

    private fun showProgress(){
        val progressDialog = ProgressDialog(this)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Loading....")
        progressDialog.show()
    }

    private fun hideProgress() {
        val progressDialog = ProgressDialog(this)
        progressDialog.hide()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.textView_mendaftar -> {
                val intent = Intent(this, RegisterActivity::class.java)
                startActivity(intent)
            }
            R.id.button_signin -> {
                showProgress()
                val email = textinput_email_login.text.toString()
                val password = textinput_password_login.text.toString()
                FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password)
                    .addOnCompleteListener {
                        if(it.isSuccessful) {
                            hideProgress()
                            val intent = Intent(this,MainActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                        }else{
                            return@addOnCompleteListener
                        }
                    }
                    .addOnFailureListener {
                        hideProgress()
                        Toast.makeText(this,"Failed to login : ${it.message}", Toast.LENGTH_SHORT).show()
                    }
            }
        }

    }
}