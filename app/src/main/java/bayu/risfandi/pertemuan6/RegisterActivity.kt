package bayu.risfandi.pertemuan6

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() ,View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        button_signup.setOnClickListener(this)
        textview_login.setOnClickListener(this)

        supportActionBar!!.title = "Register"
    }

    private fun showProgress(){
        val progressDialog = ProgressDialog(this)
        progressDialog.isIndeterminate = true
        progressDialog.setMessage("Loading....")
        progressDialog.show()
    }

    private fun hideProgress() {
        val progressDialog = ProgressDialog(this)
        progressDialog.hide()
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.button_signup -> {
                showProgress()
                val email = textinput_email_register.text.toString()
                val password = textinput_password.text.toString()
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener {
                        if(it.isSuccessful) {
                            hideProgress()
                            Toast.makeText(this, "Register Berhasil",Toast.LENGTH_LONG).show()
                            val intent = Intent(this,MainActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                        }else{
                            return@addOnCompleteListener
                        }
                    }
                    .addOnFailureListener {
                        hideProgress()
                        Toast.makeText(this,"Failed to create user: ${it.message}", Toast.LENGTH_SHORT).show()
                    }
            }
            R.id.textview_login -> {
                val intent = Intent(this,LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }
    }
}